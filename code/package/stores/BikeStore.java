package stores;
import vehicles.Bicycle;
public class BikeStore {
    
    public static void main (String[]args){
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("one",11,111);
        bikes[1] = new Bicycle("chew",22,222);
        bikes[2] = new Bicycle("tree",33,333);
        bikes[3] = new Bicycle("four",44,444);
        for (int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
        }
    }
}
