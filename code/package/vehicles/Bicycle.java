package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String man, int num, double max)
    {
        this.manufacturer = man;
        this.numberGears = num;
        this.maxSpeed = max;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    
    public int getNumberGear(){
        return this.numberGears;
    }
    
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer " + this.manufacturer + " Number of gears " + this.numberGears + " Max Speed " + this.maxSpeed;
    }
}

